---
id: install-node
title: Install Node
sidebar_label: Install Node
---

**Node installation.**

curl \-sL https://deb.nodesource.com/setup\_10.x | sudo \-E bash \-

  

sudo apt\-get install \-y nodejs

  
**Upgrade node version using ‘n’ module**

sudo npm install n \-g

  
  

sudo n 10.6.0 // for specific version (use this version)

sudo n stable // for stable version

sudo n latest // for latest version

  
  

**Check version**

node \-v

  
  
**Install PM2**

sudo npm i pm2 \-g