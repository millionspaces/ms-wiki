---
id: install-java
title: Java Installation
sidebar_label: Install Java
---

## Java Installation

#### Run the below linux commands to install the correct version of Java 1.8.0_181


    sudo add-apt-repository ppa:webupd8team/java
    sudo apt-get update
    sudo apt-get install oracle-java8-installer

Verfy the Java version by command ***java -version***


## Setting up Java HOME Variable

