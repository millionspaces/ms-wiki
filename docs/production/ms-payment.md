---
id: ms-payment-prod
title: MS payment server deployment
sidebar_label: MS-Payment
---

**1\. Clone the project git clone https://username@bitbucket.org/millionspaces/ms\-payment\-server.git**

  

**2\. Install project dependencies npm install**

  

**3\. Generate build artifacts / Run (Live/Production environment)**

**3.1 push changes to the repo**

git push origin master

  

**3.2 connect to the LIVE server and pull changes to the following directory**

cd /home/backend/ms\-payment\-server/ ↵

  

git pull origin master

  

**3.3 Copy directory into /opt/**

cd /opt/ ↵

sudo cp /home/backend/ms\-payment\-server/ . \-rf

  

**3.4 start the server using PM2**

NODE\_ENV=production pm2 start /opt/ms\-payment\-server/index.js

  

**4\. Process manager scripts**

**4.1 start application**

NODE\_ENV=development pm2 start /opt/ms\-payment\-server/index.js

NODE\_ENV=production pm2 start /opt/ms\-payment\-server/index.js

  

**4.2 list all running instances**

pm2 ls

  

**4.3 start,stop and restart process**

pm2 start

pm2 stop

pm2 restart

  

**4.4 show details of an app**

pm2 show

  

**4.5 show logs**

pm2 logs

  

**4.6 monitor process**

pm2 monit