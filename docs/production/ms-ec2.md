---
id: ms-ec2-prod
title: EC2 Configurations
sidebar_label: EC2 Configurations
---

## Swap Creation

#### Run the below to create a 1GB of swap

    sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
    sudo /sbin/mkswap /var/swap.1
    sudo chmod 600 /var/swap.1
    sudo /sbin/swapon /var/swap.1

Add the following line in the ***/etc/fstab*** file.

    /var/swap.1   swap    swap    defaults        0   0
