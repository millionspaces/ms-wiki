---
id: ms-web-prod
title: MS front-end project deployment
sidebar_label: MS-Web
---


**1\. Clone the project**

git clone https://bitbucket.org/millionspaces/ms\-web.git

  

**2\. Install project dependencies**

npm install

  

**3\. Generate build artifacts / Run (Live/Production environment)**

**3.1 inside the project root, run**

npm run prod\-live  

**3.2 move ‘dist’ into QAE server**


cd live\-build/dist ↵

  

rsync \-avzhe ssh \-\-progress \-\-delete . username@54.169.56.172:/home/username/live\-build/dist/

  

**3.3 sync with S3 and following commands should be run being inside QA server (54.169.56.172)**

s3cmd \-\-exclude '.git/\*' sync \-\-delete\-removed /home/username/live\-build/dist/ s3://millionspaces.com/ ↵

  

s3cmd \-\-recursive modify \-\-add\-header='content\-type':'text/css' \-\-exclude '' \-\-include '.css' s3://millionspaces.com/

  

**3.4 Invalidate CloudFront**
