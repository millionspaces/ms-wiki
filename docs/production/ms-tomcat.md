---
id: ms-tomcat-prod
title: MS tomcat server deployment
sidebar_label: MS-Tomcat
---

## Tomcat server installation and configuration

### Installation

##### Pre Requirements

Java version

Java home


#### Installation steps

First download the Tomcat version 7 - tar.gz file from the [official tomcat site](http://www-eu.apache.org/dist/tomcat/tomcat-7/v7.0.90/bin/apache-tomcat-7.0.90.tar.gz).

Change the current directory to the /opt.

Extract the downloaded archive using command (*tar -xzf tomcat-directory.tar.gz*)

Set the permission to the *ubuntu* user using the command (*chown -R ubuntu:ubuntu tomcat-directory*)

You can check whether the the tomcat is working properly by running the *startup.sh* script inside the *bin* directory.


### Configuration

**Add the below two code segments in the configuration file *server.xml* located at directory */opt/tomcat-directory/conf/***

1)

        <Connector port="8080" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443"
               proxyPort="443"
               scheme="https"/>
2)


        <Host name="localhost"  appBase="eventspace"
                unpackWARs="true" autoDeploy="true">

        <Context path="" docBase=""/>