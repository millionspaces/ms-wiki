---
id: ms-server-prod
title: MS Backend Deployment and Verification Script
sidebar_label: MS-Backend-Server
---

### Project deployment

Pull the latest version of the project

	cd /home/backend/ms-spring-server
	sudo git pull
Remove the existing build ***target*** and run the new maven build as ***live-master*** for live-master build. 

	sudo rm -rf /home/backend/ms-spring-server/eventspace-web/target
	cd /home/backend/ms-spring-server
	sudo mvn clean install -P live-master
For *live* build it is required to run the same commands using ***live*** instead of ***live-master***

	sudo rm -rf /home/backend/ms-spring-server/eventspace-web/target
	cd /home/backend/ms-spring-server
	sudo mvn clean install -P live

Move inside the *target* directory and extract the new build ***eventspace.war*** file.

	cd /home/backend/ms-spring-server/eventspace-web/target
	sudo unzip -d eventspace eventspace.war

Copy the running project to the backup directory. 

	sudo cp /opt/tomcat-7.0/eventspace /home/backup -rf

Shutdown the tomcat server, remove the existing project and look into the log file.

	sudo /opt/tomcat-7.0/bin/shutdown.sh
	sudo rm /opt/tomcat-7.0/eventspace -rf
	tail -f /opt/tomcat-7.0/logs/catalina.out

Copy the newly extracted build files in the tomcat server and start the server.

	sudo cp /home/backend/ms-spring-server/eventspace-web/target/eventspace /opt/tomcat-7.0 -rf
	sudo /opt/tomcat-7.0/bin/startup.sh
	tail -f /opt/tomcat-7.0/logs/catalina.out
**In a situation where the new deployment has errors revert back the system to the previous state**

	sudo /opt/tomcat-7.0/bin/shutdown.sh
	sudo rm /opt/tomcat-7.0/eventspace -rf
	tail -f /opt/tomcat-7.0/logs/catalina.out

	sudo cp /home/backup/eventspace /opt/tomcat-7.0 -rf
	sudo /opt/tomcat-7.0/bin/startup.sh
	tail -f /opt/tomcat-7.0/logs/catalina.out

### Verify the new deployment

Run couple of *curl* commands to verify the new deployment has running seamlessly.

	curl -i api.millionspaces.com/api/common/system

	curl -i api.millionspaces.com/api/space/177