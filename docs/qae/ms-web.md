---
id: ms-web-qae
title: MS front-end project deployment
sidebar_label: MS-Web
---


**1\. Clone the project**

git clone https://bitbucket.org/millionspaces/ms\-web.git

  

**2\. Install project dependencies**

npm install

  

**3\. Generate build artifacts / Run (QA environment)**

**3.1 Inside the project root, run**

npm run prod\-qae

  

**3.2 move ‘dist’ into QAE server**

cd qae\-build/dist ↵

  

rsync \-avzhe ssh \-\-progress \-\-delete .

username@54.169.56.172:/home/username/qae\-build/dist/

  

**3.4 sync with S3 and following commands should be run being inside QA server (54.169.56.172)**

s3cmd \-\-exclude '.git/\*' sync \-\-delete\-removed /home/username/qae\-build/dist/ s3://qae.millionspaces.com/ ↵

  

  

**3.5 Invalidate CloudFront**